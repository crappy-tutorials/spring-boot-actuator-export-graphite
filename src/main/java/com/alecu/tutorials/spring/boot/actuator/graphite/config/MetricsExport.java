package com.alecu.tutorials.spring.boot.actuator.graphite.config;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.Metric;
import org.springframework.boot.actuate.metrics.repository.MetricRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class MetricsExport {

    @Autowired private MetricRepository repository;

    @Scheduled(fixedRate = 1000)
    private void exportMetrics() {
        repository.findAll().forEach(this::saveMetric);
    }

    private void saveMetric(Metric<?> m) {
        System.out.println(String.format("%s = %s", m.getName(), m.getValue()));

        try {
            Socket socket = new Socket("104.236.218.223", 2003);
            OutputStream stream = socket.getOutputStream();
            PrintWriter out = new PrintWriter(stream, true);
            out.printf("%s %d %d%n", m.getName(), m.getValue().intValue(), System.currentTimeMillis() / 1000);
            out.close();
            socket.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        repository.reset(m.getName());
    }

}
